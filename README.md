# fsnapmgr

Basic Snap Manager by frknkrc44

## Functions

* download Snap packages
* search Snap packages

## Credits

* [@sulincix](https://gitlab.com/sulincix/outher/-/blob/gh-pages/snap-installer.sh)
* [Snap Store](https://snapcraft.io/)
* My brain
