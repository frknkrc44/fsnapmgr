from requests import get
from bs4 import BeautifulSoup as bs
from re import sub
from sys import argv, stdout
from shutil import get_terminal_size as gts
from json import loads

class Package:
	
	def __init__(self, pkg_title, pkg_link, pkg_name, pkg_pub):
		self.title = pkg_title
		self.link = pkg_link
		self.name = pkg_name
		self.pub = pkg_pub

def prnt(line='', end='\n'):
	stdout.write(f'{line}{end}')

def get_query(query, print_query=False, print_only_name=False):
	ret = None if print_query else []
	req = get(f'https://snapcraft.io/search?q={query}')
	soup = bs(req.text, 'html.parser')

	res = soup.findAll('a', {'class':['p-media-object','p-media-object--snap']})

	if len(res) < 1:
		return ret
	
	for item in res:
		name = item['href'][1:]
		if print_only_name:
			prnt(name)
		else:
			link = f"https://snapcraft.io{item['href']}"
			title = item['title']
			pub = item.find('div', {'class':['p-media-object__content']})
			pub = pub.find('p')
			pubtext = sub('Publisher:', '', pub.text).strip()
			if pub.find('span', {'class':['p-verified']}):
				pubtext += ' (Verified)'
			if print_query:
				prnt(f'Name: {name}\nTitle: {title}\nLink: {link}\nPublisher: {pubtext}\n')
			else:
				ret.append(Package(title, link, name, pubtext))
	
	return ret

def print_help(exit_code=1):
	prnt('fsnapmgr - Basic Snap Manager by frknkrc44')
	prnt(f'Usage: {argv[0]} <option> <package>')
	prnt('\nOptions:')
	prnt('download - download a package from Snap Store')
	prnt('search   - search a package from Snap Store')
	exit(exit_code)

def print_search_result(query):
	get_query(query, print_query=True)

def download_package(query):
	url = f'https://search.apps.ubuntu.com/api/v1/search?q={query}'
	req = get(url)
	json = loads(req.text)
	if len((idx := json['_embedded']['clickindex:package'])) < 1:
		prnt(f'{query} not found in repository')
		prnt('Similar packages:')
		get_query(query, print_query=True, print_only_name=True)
		exit(1)
	
	idx = idx[0]
	link = None
	if 'anon_download_url' in idx:
		link = idx['anon_download_url']
	elif 'download_url' in idx:
		link = idx['download_url']
	else:
		prnt('Abnormal status, please check it manually')
		prnt(url)
		exit(1)
	
	filename = f'{query}'
	if 'architecture' in idx:
		filename += f'_{idx["architecture"][0]}'
	filename += '.snap'
	with open(filename,'wb') as snap:
		prnt(f'Downloading to {filename} ...')
		prnt(f'Release {idx["release"][0]}, revision {idx["revision"]}')
		prnt(f'License: {idx["license"]}')
		prnt(f'Summary: {idx["summary"]}')
		prnt(f'Publisher: {idx["developer_name"]}', end='')
		prnt(' (Verified)' if idx['developer_validation'] == 'verified' else '')
		prnt(f'SHA512: {idx["download_sha512"]}')
		
		res = get(link, stream=True)
		total = res.headers.get('content-length')
		if not total:
			snap.write(res.content)
		else:
			down = 0
			total = int(total)
			for data in res.iter_content(chunk_size=4096):
				down += len(data)
				snap.write(data)
				step = gts()[0] - 2 # give width of terminal every time
				prg = int(step * down / total)
				prnt(f'\r[{"+" * prg}{" " * (step - prg)}]', '')
		prnt(f"\rDone!{' ' * (step - 3)}")

if len(argv) < 2:
	print_help()

if 'search' == argv[1]:
	print_help() if len(argv) < 3 or len(argv[2].strip()) < 1 else print_search_result(argv[2])

elif 'download' == argv[1]:
	print_help() if len(argv) < 3 or len(argv[2].strip()) < 1 else download_package(argv[2])

else:
	print_help()
